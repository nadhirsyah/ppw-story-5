from django.shortcuts import render, redirect
from .models import Schedule
from . import forms

def home(request):
    return render(request, "home.html")

def about(request):
    return render(request, "about.html")

def experience(request):
    return render(request, "experience.html")

def registration(request):
    return render(request, "registration.html")

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")


