from django.urls import path
from . import views

urlpatterns = [
    path('', views.home,  name="home"),
    path('about/', views.about, name="about"),
    path('experience/', views.experience, name="experience"),
    path('registration/', views.registration, name = "registration"),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
]
